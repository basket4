
/* Define if libgpgme is available */
#cmakedefine HAVE_LIBGPGME

/* Version number of package */
#define VERSION "1.0.cmake"

/* Defined if compiling without arts */
#undef WITHOUT_ARTS

